﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    Rigidbody rb;
    Renderer rend;
    private int count;
    public Text countText;
    public Text winText;
    public float speed;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winText.text = "";
        rend = GetComponent<Renderer>();


    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float moveHorizotal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizotal, 0f, moveVertical);

        rb.AddForce(movement * speed);
    }
    //pick up game object
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();



        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        //check to see if the player hit a wall
        if (collision.gameObject.CompareTag("wall"))
        {

        

        print("HIT Wall!");
        //Hit wall, set the ball color to the color material color

        rend.material = collision.gameObject.GetComponent<Renderer>().material;

        }
    }
    void SetCountText()
    {
        //when the player pick up object is less than 12 display win
        countText.text = "Count: " + count.ToString();
        if (count>= 12)
        {
            winText.text = "You Win";
        }

    }
}

