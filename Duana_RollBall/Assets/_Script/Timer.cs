﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 



public class Timer : MonoBehaviour
{
    public Text timerText;
    private float startTime;
    // Start is called before the first frame update
    void Start()
    {
        //create a new variable called Start time
        startTime = Time.time;

    }

    // Update is called once per frame
    void Update()
    {
        //count the time, substracting time Time and the start time when the player start the game
        float t = Time.time - startTime;

        //time in minute
        string minutes = ((int)t / 60).ToString();

        //give us the time with 0 decimal point
        //time in second
        string seconds = (t % 60).ToString("f0");
        //update the time
        timerText.text = minutes + ":" + seconds;
    }
}
