﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jumper : MonoBehaviour
{
    [Range(1, 10)]
    public float jumpVelocity;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update(){
        //check if the jump botton is been presss
     if (Input.GetButtonDown("Jump"))
        {
            //set velocity
            GetComponent<Rigidbody>().velocity = Vector3.up * jumpVelocity;
        }
             
    }
}
